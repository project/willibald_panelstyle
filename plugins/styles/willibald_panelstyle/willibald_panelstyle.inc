<?php

/**
 * @file
 * Panel style for the Willibald theme.
 */

/**
 * Implements hook_panels_styles().
 */
$plugin = array(
  'willibald_panelstyle' => array(
    'title' => t('Willibald panel style'),
    'description'   => t('Panel style for the Willibald theme.'),
    'render pane' => 'willibald_panelstyle_render_pane',
    'render region' => 'willibald_panelstyle_render_region',
    'pane settings form' => 'willibald_panelstyle_settings_form',
  ),
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_willibald_panelstyle_render_pane($variables) {
  return theme('willibald_pane', $variables);
}

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_willibald_panelstyle_render_region($variables) {
  return implode($variables['panes']);
}

/**
 * Form callback.
 */
function willibald_panelstyle_settings_form($settings) {
  $form['background_color'] = array(
    '#type' => 'select',
    '#title' => t('Background color'),
    '#options' => array(
      'bg-none' => t('No background color'),
      'bg-dark-blue' => t('dark blue'),
      'bg-light-blue' => t('light blue'),
      'bg-orange' => t('orange'),
      'bg-green' => t('green'),
    ),
    '#default_value' => (isset($settings['background_color'])) ? $settings['background_color'] : 'willibald-bg-orange',
  );

  $form['text_color'] = array(
    '#type' => 'select',
    '#title' => t('Text color'),
    '#options' => array(
      'tx-none' => t('No text color'),
      'tx-white' => t('white'),
      'tx-dark-blue' => t('dark blue'),
      'tx-light-blue' => t('light blue'),
      'tx-orange' => t('orange'),
      'tx-green' => t('green'),
    ),
    '#default_value' => (isset($settings['text_color'])) ? $settings['text_color'] : 'tx-white',
  );

  $form['vertical_rhythm'] = array(
    '#type' => 'select',
    '#title' => t('Use vertical rhythm'),
    '#options' => array(
      'vertical-rhythm' => t('vertical rhythm'),
      'no-vertical-rhythm' => t('no vertical rhythm'),
    ),
    '#default_value' => (isset($settings['vertical_rhythm'])) ? $settings['vertical_rhythm'] : 'vertical-rhythm',
  );

  $form['vertical_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rotate the text in the vertical direction'),
    '#description' => t('Use only short texts. The text is displayed in a headline manner.'),
    '#default_value' => (isset($settings['vertical_text'])) ? $settings['vertical_text'] : 0,
  );

  return $form;
}
