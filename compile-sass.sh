#! /bin/sh

_compile() {
  local CSS_NAME="$(echo "$1" | sed 's/.scss//g' )"

  echo "Compile sass/$CSS_NAME.scss into css/$CSS_NAME.css"
  sassc \
    --style=expanded \
    --load-path=sass-imports/willibald \
    --precision=10 \
    --sourcemap \
    sass/$CSS_NAME.scss \
    css/$CSS_NAME.css
}

if [ -n "$1" ]; then
  _compile "$1"
else
  cd sass
  CSS_NAMES="$(find . -type f ! -path "*_*")"
  cd ..

  find css -iname "*.css" -exec rm -f {} \;

  for CSS_NAME in $CSS_NAMES; do
      _compile "$CSS_NAME"
  done
fi
