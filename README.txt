The module provides a panelstyle for the Willibald theme
(http://drupal.org/project/willibald). It allows to change the background of the
panels in four different colors.

  * dark blue
  * light blue
  * orange
  * green

The text can be changed to five colors:

  * white
  * dark blue
  * light blue
  * orange
  * green

Dependencies
============
  * ctools
  * panels

Development
===========

You must have installed sass. Run ./compile.sass.sh to generate the css files.

MAINTAINERS
-----------

Current maintainers:
 * Josef Friedrich (joseffriedrich) - https://drupal.org/user/580678
